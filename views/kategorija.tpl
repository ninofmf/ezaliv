<!DOCTYPE html>
<html>
  <head>
    <title>eZaliv</title>
    <meta charset="utf-8">
  </head>
  <body>
    <header>
      <h1>eZaliv</h1>
    </header>

    <section id="vnos">
      <h2>Kategorija: <i>{{ime}}</i> </h2>
% for id_, izdelek in izdelki.items():
      <div style="background-color: #d3d3d3; margin-bottom: 10px;">
        <p><b>{{izdelek['ime']}}</b> za samo {{izdelek['cena']}} EUR <br />
        Zaloga: {{izdelek['zaloga']}}</p>
        <p>{{izdelek['opis']}}</p>
      </div>
% end
    </section>
    
    <hr />

    <section id="vnos">
      <p> <a href="/admin">Vstop za administratorja</a> </p>
    </section>

    
    <footer>  
    </footer>

  </body>
</html>
